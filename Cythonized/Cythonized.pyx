from libc.math cimport sin, cos, sqrt
import numpy as np
cimport numpy as np
cimport cython


@cython.boundscheck(False)
@cython.wraparound(False)
cpdef tuple rotation_matrix(double theta, double x, double y, double z):
    """
    Compute the rotation matrix for rotation around an axis by theta radians.
    TODO: Return a C-type instead of list to increase performance.
    """
    cdef double div, a, b, c, d

    div = sqrt(x**2 + y**2 + z ** 2)
    x = x / div
    y = y / div
    z = z / div
    a = cos(theta / 2.0)
    b = -x * sin(theta / 2.0)
    c = -y * sin(theta / 2.0)
    d = -z * sin(theta / 2.0)

    #cdef np.ndarray[double, ndim=2] rt = np.empty([3, 3], dtype=np.double)
    #rt[0][0] = a * a + b * b - c * c - d * d
    #rt[0][1] = 2 * (b * c + a * d)
    #rt[0][2] = 2 * (b * d - a * c)
    #rt[1][0] = 2 * (b * c - a * d)
    #rt[1][1] = a * a + c * c - b * b - d * d
    #rt[1][2] = 2 * (c * d + a * b)
    #rt[2][0] = 2 * (b * d + a * c)
    #rt[2][1] = 2 * (c * d - a * b)
    #rt[2][2] = a * a + d * d - b * b - c * c
    #return rt

    return (
        (
            a * a + b * b - c * c - d * d,
            2 * (b * c + a * d),
            2 * (b * d - a * c)
        ),
        (
            2 * (b * c - a * d),
            a * a + c * c - b * b - d * d,
            2 * (c * d + a * b)
        ),
        (
            2 * (b * d + a * c),
            2 * (c * d - a * b),
            a * a + d * d - b * b - c * c
        )
    )