

import math


class Point2D(object):
    def __init__(self, x, y):
        super(Point2D, self).__init__()
        self._c = [x, y]

    def __repr__(self):
        return '{}.{}(*{})'.format(self.__module__, self.__class__.__name__, self._c)

    def __iter__(self):
        return self._c.__iter__()

    def __len__(self):
        return self._c.__len__()

    def __getitem__(self, i):
        return self._c[i]

    def __add__(self, other):
        return Point2D(self._c[0] + other._c[0], self._c[1] + other._c[1])

    def __sub__(self, other):
        return Point2D(self._c[0] - other._c[0], self._c[1] - other._c[1])

    @property
    def X(self):
        return self._c[0]

    @X.setter
    def X(self, value):
        self._c[0] = value

    @property
    def Y(self):
        return self._c[1]

    @Y.setter
    def Y(self, value):
        self._c[1] = value

    def det(self, p):
        """
        Compute the determinant of two Point2D objects
        :param p: Point2D
        :return: int or float
        """
        return self._c[0] * p.Y - self._c[1] * p.X

    def distance(self, p):
        """
        Compute the distance between two Point2D
        :param p: Point2D
        :return: int or float
        """
        return math.sqrt(math.pow(self._c[1] - p.Y, 2) + math.pow(self._c[0] - p.X, 2))

    def move(self, vector):
        """
        Move a Point2D by a Vector2D
        :param vector: Vector2D
        :return: None
        """
        self._c[0] += vector.X
        self._c[1] += vector.Y
        return self


class Vector2D(object):
    def __init__(self, x, y):
        super(Vector2D, self).__init__()
        self._c = (x, y)

    def __repr__(self):
        return '{}.{}(*{})'.format(self.__module__, self.__class__.__name__, self._c)

    def __iter__(self):
        return self._c.__iter__()

    def __len__(self):
        self._c.__len__()

    def __getitem__(self, i):
        return self._c[i]

    def __mul__(self, other):
        if isinstance(other, (Vector2D, Point2D)):
            return Vector2D(self.c_[0] * other.X, self._c[1] * other.Y)
        elif isinstance(other, (int, float)):
            return Vector2D(self._c[0] * other, self._c[1] * other)

    def __add__(self, other):
        if isinstance(other, (Point2D, Vector2D)):
            return Vector2D(self._c[0] + other.X, self._c[1] + other.Y)
        elif isinstance(other, (int, float)):
            return Vector2D(self._c[0] + other, self._c[1] + other)

    @property
    def X(self):
        return self._c[0]

    @property
    def Y(self):
        return self._c[1]

    @property
    def magnitude(self):
        """
        Compute the Vector2D magnitude
        :return: float
        """
        return math.sqrt(math.pow(self._c[0], 2) + math.pow(self._c[1], 2))

    def dot(self, vector):
        """
        Compute the dot product of two Vector2D
        :param vector: Vector2D
        :return: int or float
        """
        return self._c[0] * vector.X + self._c[1] * vector.Y

    def det(self, vector):
        """
        Compute the determinant of two Vector2D
        :param vector: Vector2D
        :return: int or float
        """
        return self._c[0] * vector.Y - self._c[1] * vector.X

    def ccw_angle(self, vector):
        """
        Compute the ccw-angle between two Vector2D
        :param vector:
        :return: int or float
        """
        return math.atan2(self.det(vector), self.dot(vector))

    def shortest_angle(self, vector):
        """
        Compute the shortest angle between two Vector2D
        :param vector: Vector2D
        :return: radians
        """
        return math.acos(self.dot(vector))

    def rotate(self, theta):
        """
        Return a Vector2D rotated by theta radians
        :param theta: radians
        :return: Vector2D
        """
        return Vector2D(math.cos(theta) * self._c[0] - math.sin(theta) * self._c[1],
                        math.sin(theta) * self._c[0] + math.cos(theta) * self._c[1])

    def unitize(self, magnitude=1):
        """
        Return a unitized Vector2D
        :return: Vector2D
        """
        return Vector2D(self._c[0] / self.magnitude * magnitude, self._c[1] / self.magnitude * magnitude)

    def in_quad(self):
        """
        Returns the quadrant in which the vector points
        :return: int
        """
        if self._c[0] > 0 and self._c[1] > 0:
            return 1
        elif self._c[0] < 0 and self._c[1] < 0:
            return 3
        elif self._c[0] < 0 < self._c[1]:
            return 2
        elif self._c[0] > 0 > self._c[1]:
            return 4

    @classmethod
    def from2pts(cls, a, b):
        """
        Construct a Vector2D between two Point2D
        :param a: Point2D
        :param b: Point2D
        :return: Vector2D
        """
        return cls(*b - a)

    @classmethod
    def from_angle(cls, angle, magnitude=1):
        """
        Construct a Vector2D from an angle and an optional magnitude
        :param angle: radians
        :param magnitude: int or float
        :return: Vector2D
        """
        return cls(1, 0).rotate(angle) * magnitude


class Plane(object):
    def __init__(self, origin, x_axis, y_axis):
        super(Plane, self).__init__()
        self.origin = origin
        self._axis = x_axis, y_axis

    def __repr__(self):
        return '{}.{}({}, {})'.format(self.__module__, self.__class__.__name__, self.origin, *self._axis)

    @property
    def XAxis(self):
        return self._axis[0]

    @property
    def YAxis(self):
        return self._axis[1]

    @property
    def ZAxis(self):
        return self._axis[0].cross(self._axis[1])

    def rotate(self, theta, axis):
        """
        Rotate a plane around an axis by theta radians
        :param theta: radians
        :param axis: Vector3D
        :return: Plane
        """
        return Plane(
            self.origin,
            self._axis[0].rotate(theta, axis),
            self._axis[1].rotate(theta, axis)
        )

    @classmethod
    def from_3pts(cls, pts):
        """
        Construct a plane from a list or tuple of 3 Point3D as: Plane(lst[0], lst[1] - lst[0], lst[2] - lst[0])
        :param pts: list or tuple
        :return: Plane
        """
        x = Vector3D.from2pts(pts[0], pts[1]).unitize()
        y = Vector3D.from2pts(pts[0] + x * Vector3D.from2pts(pts[0], pts[2]).dot(x), pts[2]).unitize()
        return cls(pts[0], x, y)
