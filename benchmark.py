

class v1(object):
    def __init__(self, x, y, z):
        self.X = x
        self.Y = y
        self.Z = z
        self._i = 0

    def __len__(self):
        return 3

    def __iter__(self):
        return self

    def next(self):
        if self._i >= self.__len__():
            raise StopIteration
        else:
            self._i += 1
            return [self.X, self.Y, self.Z][self._i - 1]

    def __getitem__(self, item):
        lst = [self.X, self.Y, self.Z]
        return lst[item]


class v2(object):
    def __init__(self, x, y, z):
        self._c = (x, y, z)

    def __len__(self):
        return self._c.__len__()

    def __iter__(self):
        return self._c.__iter__()

    def __getitem__(self, i):
        return self._c[i]

    @property
    def X(self):
        return self._c[0]

    @property
    def Y(self):
        return self._c[1]

    @property
    def Z(self):
        return self._c[2]
