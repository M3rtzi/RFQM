"""
RFQM Graph generator lib based on principles from 'Rigidly Foldable Quadrilateral Meshes from Angle Arrays' by Robert J.
Lang and Larry Howell.

Version: .4

Written by Jakob

# TODO: issue with uniform node length occurs in _gen_matrix2() under #all other nodes. Issue may inherit from line but could also be an issue with vector enumeration/indexing.
# TODO: Lines may be avoided, intersection func may be written to take points and vectors
# TODO: Detect self intersecting graph.
# TODO: Edge length optimization routines.
# TODO: Issues with graphs when V-count > U-count. find out why!
"""

import math
import json


# GEOMETRY CLASSES:
class Point2D(object):
    count = 0
    def __init__(self, x, y):
        super(Point2D, self).__init__()
        self.coords = [x, y]
        Point2D.count += 1

    def __repr__(self):
        return '{}.{}(*{})'.format(self.__module__, self.__class__.__name__, self.coords)

    def __iter__(self):
        return self.coords.__iter__()

    def __len__(self):
        return self.coords.__len__()

    def __getitem__(self, i):
        return self.coords[i]

    def __add__(self, other):
        return Point2D(self.coords[0] + other.coords[0], self.coords[1] + other.coords[1])

    def __sub__(self, other):
        return Point2D(self.coords[0] - other.coords[0], self.coords[1] - other.coords[1])

    @property
    def X(self):
        return self.coords[0]

    @X.setter
    def X(self, value):
        self.coords[0] = value

    @property
    def Y(self):
        return self.coords[1]

    @Y.setter
    def Y(self, value):
        self.coords[1] = value

    def det(self, p):
        """
        Compute the determinant of two Point2D objects
        :param p: Point2D
        :return: int or float
        """
        return self.coords[0] * p.Y - self.coords[1] * p.X

    def distance(self, p):
        """
        Compute the distance between two Point2D
        :param p: Point2D
        :return: int or float
        """
        return math.sqrt(math.pow(self.coords[1] - p.Y, 2) + math.pow(self.coords[0] - p.X, 2))

    def move(self, vector):
        """
        Move a Point2D by a Vector2D
        :param vector: Vector2D
        :return: None
        """
        self.coords[0] += vector.X
        self.coords[1] += vector.Y
        return self


class Vector2D(object):
    def __init__(self, x, y):
        super(Vector2D, self).__init__()
        self.coords = (x, y)

    def __repr__(self):
        return '{}.{}(*{})'.format(self.__module__, self.__class__.__name__, self.coords)

    def __iter__(self):
        return self.coords.__iter__()

    def __len__(self):
        self.coords.__len__()

    def __getitem__(self, i):
        return self.coords[i]

    def __mul__(self, other):
        if isinstance(other, (Vector2D, Point2D)):
            return Vector2D(self.c_[0] * other.X, self.coords[1] * other.Y)
        elif isinstance(other, (int, float)):
            return Vector2D(self.coords[0] * other, self.coords[1] * other)

    def __add__(self, other):
        if isinstance(other, (Point2D, Vector2D)):
            return Vector2D(self.coords[0] + other.X, self.coords[1] + other.Y)
        elif isinstance(other, (int, float)):
            return Vector2D(self.coords[0] + other, self.coords[1] + other)

    @property
    def X(self):
        return self.coords[0]

    @property
    def Y(self):
        return self.coords[1]

    @property
    def magnitude(self):
        """
        Compute the Vector2D magnitude
        :return: float
        """
        return math.sqrt(math.pow(self.coords[0], 2) + math.pow(self.coords[1], 2))

    def dot(self, vector):
        """
        Compute the dot product of two Vector2D
        :param vector: Vector2D
        :return: int or float
        """
        return self.coords[0] * vector.X + self.coords[1] * vector.Y

    def det(self, vector):
        """
        Compute the determinant of two Vector2D
        :param vector: Vector2D
        :return: int or float
        """
        return self.coords[0] * vector.Y - self.coords[1] * vector.X

    def ccw_angle(self, vector):
        """
        Compute the ccw-angle between two Vector2D
        :param vector:
        :return: int or float
        """
        return math.atan2(self.det(vector), self.dot(vector))

    def shortest_angle(self, vector):
        """
        Compute the shortest angle between two Vector2D
        :param vector: Vector2D
        :return: radians
        """
        return math.acos(self.dot(vector))

    def rotate(self, theta):
        """
        Return a Vector2D rotated by theta radians
        :param theta: radians
        :return: Vector2D
        """
        return Vector2D(math.cos(theta) * self.coords[0] - math.sin(theta) * self.coords[1],
                        math.sin(theta) * self.coords[0] + math.cos(theta) * self.coords[1])

    def unitize(self, magnitude=1):
        """
        Return a unitized Vector2D
        :return: Vector2D
        """
        return Vector2D(self.coords[0] / self.magnitude * magnitude, self.coords[1] / self.magnitude * magnitude)

    def in_quad(self):
        """
        Returns the quadrant in which the vector points
        :return: int
        """
        if self.coords[0] > 0 and self.coords[1] > 0:
            return 1
        elif self.coords[0] < 0 and self.coords[1] < 0:
            return 3
        elif self.coords[0] < 0 < self.coords[1]:
            return 2
        elif self.coords[0] > 0 > self.coords[1]:
            return 4

    @classmethod
    def from2pts(cls, a, b):
        """
        Construct a Vector2D between two Point2D
        :param a: Point2D
        :param b: Point2D
        :return: Vector2D
        """
        return cls(*b - a)

    @classmethod
    def from_angle(cls, angle, magnitude=1):
        """
        Construct a Vector2D from an angle and an optional magnitude
        :param angle: radians
        :param magnitude: int or float
        :return: Vector2D
        """
        return cls(1, 0).rotate(angle) * magnitude


class Point3D(object):
    count = 0

    def __init__(self, x, y, z):
        self.coords = [x, y, z]
        Point3D.count += 1

    def __repr__(self):
        return '{}.{}{}'.format(self.__module__, self.__class__.__name__, self.coords)

    def __add__(self, other):
        if isinstance(other, (int, float)):
            return Point3D(self.coords[0] + other, self.coords[1] + other, self.coords[2] + other)
        elif isinstance(other, (Point3D, Vector3D)):
            return Point3D(self.coords[0] + other[0], self.coords[1] + other[1], self.coords[2] + other[2])

    def __sub__(self, other):
        return Point3D(self.coords[0] - other[0], self.coords[1] - other[1], self.coords[2] - other[2])

    def __iter__(self):
        return self.coords.__iter__()

    def __len__(self):
        return self.coords.__len__()

    def __getitem__(self, i):
        return self.coords[i]

    @property
    def X(self):
        return self.coords[0]

    @X.setter
    def X(self, val):
        self.coords[0] = val

    @property
    def Y(self):
        return self.coords[1]

    @Y.setter
    def Y(self, val):
        self.coords[1] = val

    @property
    def Z(self):
        return self.coords[2]

    @Z.setter
    def Z(self, val):
        self.coords[2] = val

    def distance(self, other):
        """
        Compute the distance between two Point3D
        :param other: Point3D
        :return: int or float
        """
        return math.sqrt(
            math.pow(self.coords[0] - other.X, 2) +
            math.pow(self.coords[1] - other.Y, 2) +
            math.pow(self.coords[2] - other.Z, 2)
        )

    def move(self, v):
        """
        Move a Point3D by a Vector3D
        :param v: Vector3D
        :return: self
        """
        self.coords[0] = self.coords[0] + v.X
        self.coords[1] = self.coords[1] + v.Y
        self.coords[2] = self.coords[2] + v.Z
        return self

    @classmethod
    def from_point2d(cls, p):
        """
        Convert a Point2D in XY plane to a Point3D in XYZ space
        :param pt: Point2D
        :return: Point3D
        """
        if isinstance(p, Point2D):
            return cls(p.X, p.Y, 0)
        return p


class Vector3D(object):
    def __init__(self, x, y, z):
        super(Vector3D, self).__init__()
        self.coords = [x, y, z]

    def __repr__(self):
        return '{}.{}{}'.format(self.__module__, self.__class__.__name__, self.coords)

    def __iter__(self):
        return self.coords.__iter__()

    def __len__(self):
        self.coords.__len__()

    def __getitem__(self, i):
        return self.coords[i]

    def __add__(self, other):
        return Vector3D(self.coords[0] + other[0], self.coords[1] + other[1], self.coords[2] + other[2])

    def __sub__(self, other):
        return Vector3D(self.coords[0] - other[0], self.coords[1] - other[1], self.coords[2] - other[2])

    def __mul__(self, other):
        if isinstance(other, (Vector3D, Point3D)):
            return Vector3D(self.coords[0] * other[0], self.coords[1] * other[1], self.coords[2] * other[2])
        elif isinstance(other, (int, float)):
            return Vector3D(self.coords[0] * other, self.coords[1] * other, self.coords[2] * other)

    def __div__(self, other):
        if other != 0:
            if isinstance(other, Vector3D):
                return Vector3D(*[self.coords[i] / other[i] if other[i] != 0 else self.coords[i] for i in xrange(self.coords.__len__())])
            elif isinstance(other, (int, float)):
                return Vector3D(self.coords[0] / other, self.coords[1] / other, self.coords[2] / other)
        return self

    def __neg__(self):
        return Vector3D(-self.coords[0], -self.coords[1], -self.coords[2])

    def __eq__(self, other):
        if isinstance(other, (Vector3D, Point3D)):
            return all(
                (
                    self.coords[0] == other[0],
                    self.coords[1] == other[1],
                    self.coords[2] == other[2]
                )
            )
        elif isinstance(other, (int, float)):
            return all(
                (
                    self.coords[0] == other,
                    self.coords[1] == other,
                    self.coords[2] == other)
            )

    def __ne__(self, other):
        return not self.__eq__(other)

    @property
    def X(self):
        return self.coords[0]

    @property
    def Y(self):
        return self.coords[1]

    @property
    def Z(self):
        return self.coords[2]

    @property
    def magnitude(self):
        """
        Compute the magnitude of a Vector3D
        :return: int or float
        """
        return math.sqrt(math.pow(self.coords[0], 2) + math.pow(self.coords[1], 2) + math.pow(self.coords[2], 2))

    @magnitude.setter
    def magnitude(self, factor):
        magnitude = self.magnitude
        for i in range(self.coords.__len__()):
            if self[i] != 0:
                self.coords[i] = self.coords[i] / magnitude * factor

    def cross(self, v):
        """
        Compute the cross product of two Vector3D
        :param v: Vector3D
        :return: Vector3D
        """
        return Vector3D(
            self.coords[1] * v.coords[2] - self.coords[2] * v.coords[1],
            self.coords[2] * v.coords[0] - self.coords[0] * v.coords[2],
            self.coords[0] * v.coords[1] - self.coords[1] * v.coords[0]
        )

    def dot(self, v):
        """
        Compute the dot product of two Vector3D
        :return: float
        """
        if isinstance(v, Vector3D):
            return sum(self * v)
        else:
            raise NotImplementedError

    #def rotate(self, theta, axis):
    #    """Implementing a Cython-optimized roation matrix algorithm"""
    #    m = Cythonized.rotation_matrix(theta, axis[0], axis[1], axis[2])
    #    return Vector3D.(*[sum([m[i][j] * self[j] for j in range(len(self))]) for i in range(len(m))])

    @staticmethod
    def rotation_matrix(theta, axis):
        """
        This is 'the original', unoptimized python implementation for rollback purposes.
        :param theta: radians
        :param axis: Vector3D
        :return: rotation matrix
        """
        axis = axis / math.sqrt(axis.dot(axis))
        a = math.cos(theta / 2.0)
        b, c, d = -axis * math.sin(theta / 2.0)
        aa, bb, cc, dd = \
            a * a, \
            b * b, \
            c * c, \
            d * d
        bc, ad, ac, ab, bd, cd = \
            b * c, \
            a * d, \
            a * c, \
            a * b, \
            b * d, \
            c * d
        return [
            [
                aa + bb - cc - dd,
                2 * (bc + ad),
                2 * (bd - ac)
            ],
            [
                2 * (bc - ad),
                aa + cc - bb - dd,
                2 * (cd + ab)
            ],
            [
                2 * (bd + ac),
                2 * (cd - ab),
                aa + dd - bb - cc
            ]
        ]

    def rotate(self, theta, axis):
        """
        Rotate a Vector3D by theta radians around an axis (Vector3D)
        :param theta: radians
        :param axis: Vector3D
        :return: Vector3D
        """
        m = self.rotation_matrix(theta, axis)
        return Vector3D(*[sum([m[i][j] * self.coords[j] for j in range(len(self.coords))]) for i in range(len(m))])

    def shortest_angle(self, v):
        """
        Compute the shortest angle between two Vector3D
        :param v: Vector3D
        :return: radians
        """
        if isinstance(v, Vector3D):
            return math.acos(self.dot(v) / (self.magnitude * v.magnitude))

    def unitize(self, factor=1):
        """
        Compute the unit vector of a Vector3D
        :return: Vector3D
        """
        return Vector3D(*[self[i] / self.magnitude * factor if self[i] != 0 else self[i] for i in range(self.coords.__len__())])

    def same_dir(self, other):
        """
        Return True if Vector3D v points in the same direction as self
        :param v: Vector3D
        :return: bool
        """
        if sum(x * y >= 0 for x, y in zip(self, other)) == 3:
            return True
        return False

    @classmethod
    def from_pt(cls, p):
        """
        Construct a Vector3D from a Point3D
        :param p: Point3D
        :return: Vector3D
        """
        return cls(*p)

    @classmethod
    def from2pts(cls, a, b):
        """
        Construct a vector between two Point3D
        :param a: Point3D
        :param b: Point3D
        :return: Vector3D
        """
        if isinstance(a, Point3D) and isinstance(b, Point3D):
            return cls(*b - a)
        return cls(*Point3D.from_point2d(b) - Point3D.from_point2d(a))

    @classmethod
    def from_angle(cls, angle, axis):
        """
        Construct a Vector2D from an angle and an optional magnitude
        :param angle: radians
        :param magnitude: int or float
        :return: Vector2D
        """
        return cls(1, 0, 0).rotate(angle, axis)

    @classmethod
    def from_vector2d(cls, v):
        """
        Conctruct vector 3D from a Vector2D
        :param v: Vector2D
        :return: Vector3D
        """
        return cls(v[0], v[1], 0)

    @staticmethod
    def intersection(p1, p2, v1, v2):
        #TODO: Fails to detect cases with no intersections when lines are in different planes.
        k = v2.cross(v1)
        h = v2.cross(Vector3D.from2pts(p1, p2))
        if k.magnitude != 0 and h.magnitude != 0:
            if k.unitize() == h.unitize() or h == 0:
                return p1 + v1 * (h.magnitude / k.magnitude)
            return p1 - v1 * (h.magnitude / k.magnitude)
        return False


class Line(object):
    def __init__(self, a, b, polarity=None):
        super(Line, self).__init__()
        self.A = a
        self.B = b
        self._i = 0
        self.polarity = polarity

    def __repr__(self):
        return '{}.{}({}, {}, polarity={})'.format(self.__module__, self.__class__.__name__, self.A, self.B, self.polarity)

    def __iter__(self):
        return self

    def __len__(self):
        return 2

    def __getitem__(self, item):
        lst = [self.A, self.B]
        return lst[item]

    def next(self):
        if self._i >= self.__len__():
            raise StopIteration
        else:
            self._i += 1
            return [self.A, self.B][self._i - 1]

    @property
    def length(self):
        """
        Compute the line length
        :return: int or float
        """
        rt = self.A.distance(self.B)
        return rt

    @property
    def vector(self):
        """
        Return the direction Vector3D
        :return: Vector3D
        """
        if isinstance((self.A, self.B), Point2D):
            return Vector3D.from2pts(Point3D.from_point2d(self.A), Point3D.from_point2d(self.B))
        return Vector3D.from2pts(self.A, self.B)

    def intersection(self, line):
        """
        Returns a Point3D in the intersection between two lines if they intersect, else return False
        :param line: Line
        :return: Point3D
        """
        k = line.vector.cross(self.vector)
        h = line.vector.cross(Vector3D.from2pts(self.A, line.A))
        if k != 0:
            if k.unitize() == h.unitize() or h == 0:
                return self.A + self.vector * (h.magnitude / k.magnitude)
            return self.A - self.vector * (h.magnitude / k.magnitude)
        return False

    @classmethod
    def from_vector(cls, origin, vector, polarity=None):
        """
        Construct a Line2D from a Point2D and a Vector2D
        :param origin: Point2D
        :param vector: Vector2D
        :param polarity: bool (optional)
        :return: Line2D
        """
        if polarity is not None:
            return Line(origin, origin + vector, polarity=polarity)
        return cls(origin, origin + vector)


class Plane(object):
    def __init__(self, origin, x_axis, y_axis):
        super(Plane, self).__init__()
        self.origin = origin
        self._axis = x_axis, y_axis

    def __repr__(self):
        return '{}.{}({}, {})'.format(self.__module__, self.__class__.__name__, self.origin, *self._axis)

    @property
    def XAxis(self):
        return self._axis[0]

    @property
    def YAxis(self):
        return self._axis[1]

    @property
    def ZAxis(self):
        return self._axis[0].cross(self._axis[1])

    def rotate(self, theta, axis):
        """
        Rotate a plane around an axis by theta radians
        :param theta: radians
        :param axis: Vector3D
        :return: Plane
        """
        return Plane(
            self.origin,
            self._axis[0].rotate(theta, axis),
            self._axis[1].rotate(theta, axis)
        )

    @classmethod
    def from_3pts(cls, pts):
        """
        Construct a plane from a list or tuple of 3 Point3D as: Plane(lst[0], lst[1] - lst[0], lst[2] - lst[0])
        :param pts: list or tuple
        :return: Plane
        """
        x = Vector3D.from2pts(pts[0], pts[1]).unitize()
        y = Vector3D.from2pts(pts[0] + x * Vector3D.from2pts(pts[0], pts[2]).dot(x), pts[2]).unitize()
        return cls(pts[0], x, y)


# GRAPH CLASSES
class Graph(object):
    def __init__(self, dir_a_along_u, dir_a_along_v, fold_a_along_u, fold_a_along_v, m=0):
        super(Graph, self).__init__()
        self.m = m
        self.shape = len(dir_a_along_u), len(dir_a_along_v)
        self.dir_a_along_U = dir_a_along_u
        self.dir_a_along_V = dir_a_along_v
        self.fold_a_along_U = fold_a_along_u
        self.fold_a_along_V = fold_a_along_v
        self.plane = Plane(Point3D(0, 0, 0), Vector3D(1, 0, 0), Vector3D(0, 1, 0))
        self.nodes = []
        self.normals = [None] * (self.shape[0] - 1) * self.shape[1]
        self._gen_matrix2()

    def __repr__(self):
        params = {
            'dir_a_along_u': self.dir_a_along_U,
            'dir_a_along_v': self.dir_a_along_V,
            'fold_a_along_u': self.fold_a_along_U,
            'fold_a_along_v': self.fold_a_along_V,
            'm': self.m
        }
        return '{}.{}(**{})'.format(self.__module__, self.__class__.__name__, json.dumps(params, indent=4))

    def __str__(self):
        return '{}({}, {})'.format(self.__class__.__name__, self.shape[0], self.shape[1])

    def ToString(self):
        """
        Rhino ToString override
        """
        return self.__str__()

    def _gen_matrix(self):
        """
        this func is deprecated - _gen_matrix2() should be used instead. Will be removed after verification
        :return: None
        """
        for i in range(self.shape[0] * self.shape[1]):
            pos = self.i2pos(self.shape, i)
            if pos[0] == 0 and pos[1] == 0:  # node along left and bottom edge (first node)
                self.nodes.append(
                    NodeD4V(
                        Point2D(0, 0),
                        self.dir_a_along_V[pos[1]],
                        self.dir_a_along_U[pos[0]],
                        self.fold_a_along_V[pos[1]],
                        self.fold_a_along_U[pos[0]],
                        self
                    )
                )
            elif pos[0] == 0 and pos[1] != 0:  # node along left edge
                self.nodes.append(
                    NodeD4V(
                        self.nodes[i - self.shape[0]].origin + Vector2D.from_angle(self.nodes[i - self.shape[0]].a_out['V']),
                        self.dir_a_along_V[pos[1]],
                        self.nodes[i - self.shape[0]].a_out['V'],
                        self.fold_a_along_V[pos[1]],
                        self.nodes[i - self.shape[0]].fold_a[3],
                        self
                    )
                )
            elif pos[0] != 0 and pos[1] == 0:  # node along bottom edge
                self.nodes.append(
                    NodeD4V(
                        self.nodes[i - 1].origin + Vector2D.from_angle(self.nodes[i - 1].a_out['U']),
                        self.nodes[i - 1].a_out['U'],
                        self.dir_a_along_U[pos[0]],
                        self.nodes[i - 1].fold_a[2],
                        self.fold_a_along_U[pos[0]],
                        self
                    )
                )
            else:  # all other nodes
                origin = self.nodes[i - self.shape[0]].line_out['V'].intersection(self.nodes[i - 1].line_out['U'])
                self.nodes[i - self.shape[0]].line_out['V'].B = origin
                self.nodes[i - 1].line_out['U'].B = origin
                self.nodes.append(
                    NodeD4V(
                        origin,
                        self.nodes[i - 1].a_out['U'],
                        self.nodes[i - self.shape[0]].a_out['V'],
                        self.nodes[i - 1].fold_a[2],
                        self.nodes[i - self.shape[0]].fold_a[3],
                        self
                    )
                )

    def _gen_matrix2(self):
        """
        Constructs all the NodeD4V of the graph
        Nodes are generated in the order specified by the i2pos method
        :return: None
        """
        for i in range(self.shape[0] * self.shape[1]):
            pos = self.i2pos(self.shape, i)
            if pos[0] == 0 and pos[1] == 0:  # node along left and bottom edge (first node)
                self.nodes.append(
                    NodeD4V(
                        Point3D(0, 0, 0),
                        self.dir_a_along_V[pos[1]],
                        self.dir_a_along_U[pos[0]],
                        self.fold_a_along_V[pos[1]],
                        self.fold_a_along_U[pos[0]],
                        self
                    )
                )
            elif pos[0] == 0 and pos[1] != 0:  # node along left edge
                self.nodes.append(
                    NodeD4V(
                        self.nodes[i - self.shape[0]].origin + Vector3D.from_angle(
                            self.nodes[i - self.shape[0]].a_out['V'],
                            self.plane.ZAxis
                        ),
                        self.dir_a_along_V[pos[1]],
                        self.nodes[i - self.shape[0]].a_out['V'],
                        self.fold_a_along_V[pos[1]],
                        self.nodes[i - self.shape[0]].fold_a[3],
                        self
                    )
                )
            elif pos[0] != 0 and pos[1] == 0:  # node along bottom edge
                self.nodes.append(
                    NodeD4V(
                        self.nodes[i - 1].origin + Vector3D.from_angle(
                            self.nodes[i - 1].a_out['U'],
                            self.plane.ZAxis
                        ),
                        self.nodes[i - 1].a_out['U'],
                        self.dir_a_along_U[pos[0]],
                        self.nodes[i - 1].fold_a[2],
                        self.fold_a_along_U[pos[0]],
                        self
                    )
                )
            else:  # all other nodes
                origin = Vector3D.intersection(
                    self.nodes[i - self.shape[0]].origin,
                    self.nodes[i - 1].origin,
                    self.nodes[i - self.shape[0]].v3d['V'],
                    self.nodes[i - 1].v3d['U']
                )
                self.nodes[i - self.shape[0]].line_out['V'].B = origin
                self.nodes[i - 1].line_out['U'].B = origin
                self.nodes[i - self.shape[0]].v3d['V'].magnitude = self.nodes[i - self.shape[0]].origin.distance(origin)
                self.nodes[i - 1].v3d['U'].magnitude = self.nodes[i - 1].origin.distance(origin)
                self.nodes.append(
                    NodeD4V(
                        origin,
                        self.nodes[i - 1].a_out['U'],
                        self.nodes[i - self.shape[0]].a_out['V'],
                        self.nodes[i - 1].fold_a[2],
                        self.nodes[i - self.shape[0]].fold_a[3],
                        self
                    )
                )

    def _gen_3d_v(self, m):
        """
        Generate 3D vectors of the folded Graph.
        :return: None
        """
        i01 = self.pos2i(self.shape, (0, 1))
        normal = Vector3D(0, 0, 1)
        self.normals[0] = normal

        self.nodes[1].v3d['V'] = Vector3D(0, 1, 0)                             # dv_1,0
        self.nodes[i01].v3d['U'] = self.nodes[1].v3d['V'].rotate(                   # dh_0,1
            self.nodes[i01].a_out['U'] - self.nodes[1].a_out['V'],
            normal
        )
        self.nodes[0].v3d['U'] = self.nodes[1].v3d['V'].rotate(                     # dh_0,0
            self.nodes[0].a_out['U'] - self.nodes[1].a_out['V'],
            normal
        )
        self.nodes[0].v3d['V'] = self.nodes[i01].v3d['U'].rotate(                   # dv_0,0
            self.nodes[0].a_out['V'] - self.nodes[i01].a_out['U'],
            normal
        )

        # nodes along bottom edge
        for node in self.nodes[1:self.shape[0]]:
            i_above = self.pos2i(self.shape, (node.pos[0], node.pos[1] + 1))
            i_right = self.pos2i(self.shape, (node.pos[0] + 1, node.pos[1]))
            i_left = self.pos2i(self.shape, (node.pos[0] - 1, node.pos[1]))

            normal = self.normals[i_left].rotate(
                    NodeD4V.func_m(-node.fold_a[3], m),
                    node.v3d['V']
            )

            if node.index < self.shape[0] - 1:
                self.normals[node.index] = normal

            node.v3d['U'] = node.v3d['V'].rotate(
                node.a_out['U'] - node.a_out['V'],
                normal
            )
            self.nodes[i_above].v3d['U'] = node.v3d['V'].rotate(
                self.nodes[i_above].a_out['U'] - node.a_out['V'],
                normal
            )
            self.nodes[i_right].v3d['V'] = self.nodes[i_above].v3d['U'].rotate(
                self.nodes[i_right].a_out['V'] - self.nodes[i_above].a_out['U'],
                normal
            )

        # nodes along left
        for node in self.nodes[self.shape[0]::self.shape[0]]:
            i_above = self.pos2i(self.shape, (node.pos[0], node.pos[1] + 1))
            i_right = self.pos2i(self.shape, (node.pos[0] + 1, node.pos[1]))
            i_under = self.pos2i(self.shape, (node.pos[0], node.pos[1] - 1))
            normal = self.normals[i_under - i_under // self.shape[0]].rotate(
                NodeD4V.func_m(node.fold_a[2], m),
                node.v3d['U']
            )
            self.normals[node.index - node.index // self.shape[0]] = normal
            node.v3d['V'] = node.v3d['U'].rotate(
                 node.a_out['V'] - node.a_out['U'],
                normal
            )
            self.nodes[i_right].v3d['V'] = node.v3d['U'].rotate(
                self.nodes[i_right].a_out['V'] - node.a_out['U'],
                normal
            )

            try:
                self.nodes[i_above].v3d['U'] = self.nodes[i_right].v3d['V'].rotate(
                    self.nodes[i_above].a_out['U'] - self.nodes[i_right].a_out['V'],
                    normal
                )
            except IndexError:
                pass

        intr = [n for n in self.nodes][self.shape[0] + 1: self.shape[0] * self.shape[1]]
        del intr[self.shape[0] - 1::self.shape[0]]
        # all internal nodes
        for node in intr:
            i_above = self.pos2i(self.shape, (node.pos[0], node.pos[1] + 1))
            i_right = self.pos2i(self.shape, (node.pos[0] + 1, node.pos[1]))
            i_left = self.pos2i(self.shape, (node.pos[0] - 1, node.pos[1]))
            normal = self.normals[i_left - i_left // self.shape[0]].rotate(
                NodeD4V.func_m(-node.fold_a[3], m),
                node.v3d['V']
            )
            if (node.index - node.index // self.shape[0]) % (self.shape[0] - 1) != 0:
                self.normals[node.index - node.index // self.shape[0]] = normal

            if i_right % self.shape[0] != 0:
                self.nodes[i_right].v3d['V'] = node.v3d['U'].rotate(
                    self.nodes[i_right].a_out['V'] - node.a_out['U'],
                    normal
                )
            try:
                self.nodes[i_above].v3d['U'] = node.v3d['V'].rotate(
                    self.nodes[i_above].a_out['U'] - node.a_out['V'],
                    normal
                    )
            except IndexError:
                pass

    def fold(self, m):
        """
        Fold the graph in 3D by a percentage.
        :param m: int or float
        :return: self
        """
        self.m = m
        self._gen_3d_v(m)
        for node in self.nodes:
            if node.pos[0] == 0 and node.pos[1] == 0:
                node.origin = Point3D.from_point2d(node.origin)
            elif node.pos[0] == 0 and node.pos[1] != 0:
                node.origin = Point3D.from_point2d(self.nodes[node.index - self.shape[0]].origin) \
                              + self.nodes[node.index - self.shape[0]].v3d['V']
            elif node.pos[0] != 0 and node.pos[1] == 0:
                node.origin = Point3D.from_point2d(self.nodes[node.index - 1].origin) \
                              + self.nodes[node.index - 1].v3d['U']
            else:
                node.origin = Point3D.from_point2d(self.nodes[node.index - 1].origin) \
                              + self.nodes[node.index - 1].v3d['U'] * self.nodes[node.index - 1].line_out['U'].length
        return self

    def fold2(self, m):
        """
        Fold the graph in 3D by a percentage.
        :param m: int or float
        :return: self
        """
        self.m = m
        self._gen_3d_v(m)
        for node in self.nodes:
            if node.pos[0] == 0 and node.pos[1] == 0:
                node.origin = Point3D.from_point2d(node.origin)
            elif node.pos[0] == 0 and node.pos[1] != 0:
                node.origin = self.nodes[node.index - self.shape[0]].origin \
                              + self.nodes[node.index - self.shape[0]].v3d['V']
            elif node.pos[0] != 0 and node.pos[1] == 0:
                node.origin = self.nodes[node.index - 1].origin \
                              + self.nodes[node.index - 1].v3d['U']
            else:
                node.origin = self.nodes[node.index - 1].origin \
                              + self.nodes[node.index - 1].v3d['U']
        return self

    @classmethod
    def miura_ori(cls, u=10, v=10):
        """
        Constructs a Graph with a miura ori fold pattern
        :param u: int
        :param v: int
        :return: Graph
        """
        dirs_along_u = cls.gen_a_arr(u, 90)
        dirs_along_v = cls.gen_a_arr(v, 30)
        folds_along_u = cls.every_other(cls.gen_a_arr(u, 53.13))
        folds_along_v = cls.every_other(cls.gen_a_arr(v, -90))
        return cls(dirs_along_u, dirs_along_v, folds_along_u, folds_along_v)
    
    @staticmethod
    def every_other(t):
        """
        Inverts every other value
        :param t: tuple
        :return: tuple
        """

        return tuple(-t[i] if i % 2 == 0 else t[i] for i in xrange(len(t)))

    @staticmethod
    def gen_a_arr(n, degrees):
        """
        Returns a n-length tuple of degrees converted to radians
        :param n: int
        :param degrees: degrees
        :return: tuple
        """
        return tuple([math.radians(degrees)] * n)

    @staticmethod
    def i2pos(shape, i):
        """
        Compute matrix position from index and graph shape
        Currently bottom up, row by row, from left to right.
        :param shape: int
        :param i: int
        :return: tuple
        """
        v = i // shape[0]
        u = i - v * shape[0]
        return u, v

    @staticmethod
    def pos2i(shape, pos):
        """
        Compute the index from matrix position and graph shape
        :param shape: tuple
        :param pos: tuple
        :return: int
        """
        return shape[0] * pos[1] + pos[0]


class NodeD4V(object):
    def __init__(self, origin, u_dir_a, v_dir_a, u_fold_a, v_fold_a, graph=None):
        super(NodeD4V, self).__init__()
        self.origin = origin
        self.u_dir_a = u_dir_a
        self.v_dir_a = v_dir_a
        self.u_fold_a = u_fold_a
        self.v_fold_a = v_fold_a
        self.graph = graph
        self.index = len(self.graph.nodes)
        self.pos = Graph.i2pos(self.graph.shape, self.index)
        self.u_major, \
            self.fold_a = self.gen_fold_arr(self.u_fold_a, self.v_fold_a)
        self.sector_a = self.gen_sector_arr(self.u_dir_a, self.v_dir_a, self.u_major)
        self.a_out = {
            'U': self.eval_selfx(self.u_dir_a + math.pi - (self.sector_a[2] + self.sector_a[3]), 0),
            'V': self.eval_selfx(self.v_dir_a + math.pi - (self.sector_a[0] + self.sector_a[3]), 1)
        }
        self.v3d = {
            'U': Vector3D.from_angle(self.u_dir_a + math.pi - (self.sector_a[2] + self.sector_a[3]), self.graph.plane.ZAxis),
            'V': Vector3D.from_angle(self.v_dir_a + math.pi - (self.sector_a[0] + self.sector_a[3]), self.graph.plane.ZAxis)
        }
        self.line_out = {
            'U': Line.from_vector(
                self.origin,
                Vector3D.from_angle(self.a_out['U'], Vector3D(0, 0, 1)),
                polarity=self.fold_a[2] > 0
            ),
            'V': Line.from_vector(
                self.origin,
                Vector3D.from_angle(self.a_out['V'], Vector3D(0, 0, 1)),
                polarity=self.fold_a[3] > 0
            )
        }

    def __repr__(self):
        return '{}.{}({}, {}, {}, {}, {}, {})'.format(
            self.__module__,
            self.__class__.__name__,
            self.origin,
            self.u_dir_a,
            self.v_dir_a,
            self.u_fold_a,
            self.v_fold_a,
            repr(self.graph)
        )

    def __str__(self):
        return '{}'.format(self.__class__.__name__)

    @property
    def line_in(self):
        return (
            Line.from_vector(
                self.origin,
                Vector2D.from_angle(self.u_dir_a + math.pi),
                polarity=self.fold_a[0] > 0
            ),
            Line.from_vector(
                self.origin,
                Vector2D.from_angle(self.v_dir_a + math.pi),
                polarity=self.fold_a[1] > 0
            )
        )

    def eval_selfx(self, a, uv):
        """
        Test for 'negative' vectors (self intersection) - other self intersecting cases should be optimizable.
        :param v: angle in radians
        :param uv: 0 for u 1 for v
        :return: Vector2D
        """
        if uv:
            if not 0 < a < math.pi:
                raise ValueError('{} self intersecting at {}'.format(self.graph, self.pos))
        else:
            if not -.5 * math.pi < a < .5 * math.pi:
                raise ValueError('{} self intersecting at {}'.format(self.graph, self.pos))
        return a

    def gen_fold_arr(self, u_fold_a, v_fold_a):
        """
        Compute all the fold angles around the NodeD4V
        Note: No horizontal fold_a magnitude can be equal to any vertical fold_a magnitude
        :param v_fold_a: radians
        :param u_fold_a: radians
        :return: tuple
        """
        y1 = u_fold_a
        y2 = v_fold_a
        if abs(y2) > abs(y1):  # y2 & y4 are major
            y3 = -y1
            y4 = y2
            u_major = False
            return u_major, (y1, y2, y3, y4)
        elif abs(y2) < abs(y1):  # y1 & y3 are major
            y3 = y1
            y4 = -y2
            u_major = True
            return u_major, (y1, y2, y3, y4)
        else:
            raise ValueError('Adjacent fold angles of equal magnitude are not allowed, try a different input.')

    def gen_sector_arr(self, u_dir_a, v_dir_a, u_major):
        """
        Compute all the sector angles from the x-axis around a NodeD4V
        :param u_dir_a: radians
        :param v_dir_a: radians
        :param u_major: bool
        :return: tuple
        """

        a1 = v_dir_a - u_dir_a
        u_12 = self.fold_a_mul(self.fold_a[0], self.fold_a[1])

        if u_major:  # Horizontal major fold
            a4 = 2 * math.acos(
                abs(1 + u_12) * math.cos(.5 * a1) /
                (math.sqrt(1 + math.pow(u_12, 2) + (2 * u_12 * math.cos(a1))))
            )
            a3 = math.pi - a1
            a2 = math.pi - a4

        else:  # Vertical major fold
            a2 = 2 * math.acos(
                abs(1 + u_12) * math.cos(.5 * a1) /
                (math.sqrt(1 + math.pow(u_12, 2) + (2 * u_12 * math.cos(a1))))
            )
            a3 = math.pi - a1
            a4 = math.pi - a2
        return a1, a2, a3, a4

    def fold_a_mul(self, y_j, y_i):
        """
        Compute the fold angle multiplier two fold angles of a NodeD4V
        :param y_j: radians
        :param y_i: radians
        :return: int or float
        """
        return math.tan(.5 * y_j) / math.tan(.5 * y_i)

    @staticmethod
    def func_m(fold_a, m):
        """
        Compute fold angle at parameter m of the folded form
        :param fold_a: radians
        :param m: int or float
        :return: int or float
        """
        return 2 * math.atan(
            m * math.tan(fold_a / 2)
        )


# IF RUN AS SCRIPT:G
if __name__ == '__main__':
    g = Graph.miura_ori(10, 10, 0)