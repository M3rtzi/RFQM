import RFQM
import cProfile
import pstats


def profile(filename):
    cProfile.run(
        'RFQM.Graph.miura_ori(500, 500)',
        'profiler_files\{}.txt'.format(filename),
        sort='tottime'
    )


def load_profile(filename, amt=50):
    p = pstats.Stats('profiler_files\{}.txt'.format(filename))
    p.strip_dirs()
    p.sort_stats('ncalls')
    p.print_stats(amt)

g = RFQM.Graph.miura_ori(10, 10)

